<?php

/**
 * @return PDO
 */

function db_connect(): PDO
{
    $db = 'mysql:host='. DB_HOST .';dbname='. DB_NAME.';charset=utf8mb4';
    $options = array(
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC);
    return new PDO($db, DB_USER, DB_PASSWORD, $options);
}
