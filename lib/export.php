<?php

function export(object $user, string $field ) : void
{
    ob_clean();
    if ($field == 'json') {
        $filename = $user->id ;
        // Envoi des headers HTTP au browser pour le téléchargement du fichier.
        header('Content-type:application/json');
        // output du contenu au format json
        echo json_encode($user);
    } elseif ($field == 'csv') {
        $filename = 'user_';

        // Envoi des headers HTTP au browser pour le téléchargement du fichier.
        header('Content-type: text/csv');
        header('Cache-Control: no-store');

        $buffer = fopen('php://output', 'r+');
        fputcsv($buffer, [$user]);
        fclose($buffer);
    } else {
        header(http_response_code(500));
        die();
    }
    $filename .='_' . time() . '.' . $field;
    header('Content-disposition: attachment; filename="' . $filename . '"');
    flush();
    die();
}