<?php
function verifIndex(): void
{
    $url= $_SERVER['REQUEST_URI'] ;
    if (!str_contains($url, 'index.php')) {
        header('Location: index.php');
        die;
    }
}

/**
 * @return void
 */
function logout(): void
{
    session_unset();
    session_destroy();
    session_write_close();
    header('Location: index.php');
    die;
}