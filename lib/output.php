<?php

/**
 * @param string $content
 * @return void
 */
// La function getContent permet d'accepter le contenu d'un fichier contenant l'extension se trouvant dans le tableau de la constante FILE_EXT
function getContent(string $content): void
{
    if (is_array(FILE_EXT)) {
        foreach (FILE_EXT as $extension) {
            $filename = $content . '.' . $extension;
            if (file_exists($filename)) {
                include_once $filename;
            }
        }
    }
}

/**
 * @param string $data
 * @param string $table
 * @param string $field
 * @param string $value
 * @return mixed
 */

function getDataField(string $data, string $table, string $field, string $value ) : mixed
{

    // 1 - Connexion

    $connect = db_connect();

    // 2 - Query

    $request = $connect->prepare("SELECT $data FROM $table WHERE $field = ? ");

    $params = [
        trim($value),
    ];
    // 3 - Execute

    $request->execute($params);

    // 4 - Fetch (Transformation)
    return $request->fetchObject();
}

/**
 * @param string $data
 * @param string $table
 * @return array
 */
function getData(string $data, string $table) {

    // 1 - Connexion

    $connect = db_connect();

    // 2 - Query

    $request = $connect->prepare("SELECT $data FROM $table");

    // 3 - Execute

    $request->execute();

    // 4 - Fetch (Transformation)
    $fech = array();
    while ($row = $request->fetchObject()) {
        $fech[] = $row;
    }
    return $fech;
}

function getProfile(string $field, string $value){
    $connect = db_connect();
    $request = $connect->prepare("SELECT * FROM user WHERE $field = ?");
    $params = [
        trim($value)
    ];
    $request->execute($params);
    return $request->fetchObject();
}

/**
 * @param string $msg
 * @param string $color
 * @return void
 */
function alert(string $msg, string $color): void{
    $_SESSION['alert'] = $msg;
    $_SESSION['alert-color'] = $color;
}

/**
 * @param string $login
 * @param $value
 * @return bool
 */

function userExist(string $login, $value): bool
{
    if (is_object(getDataField('*', 'user', $login, $value))) {
        return true;
    } else {
        return false;
    }
}

/**
 * @return bool
 */
function createAdmin()
{
    $connect = db_connect();
    $insert = $connect->prepare("SELECT * FROM user WHERE admin = 1");
    $insert->execute();
    if ($insert->rowCount() == 0) {
        return 1;
    } else {
        return 0;
    }


}

/**
 * @param int $status
 * @return string
 */
function getStatus(int $status): string
{
    if ($status == 0) {
        return 'Non';
    } else {
        return 'Oui';
    }
}
