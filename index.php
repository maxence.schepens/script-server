<?php
require_once 'app/session.php';


require_once 'config.php';
require_once 'lib/output.php';
require_once 'lib/pdo.php';
require_once 'lib/redirect.php';
require_once 'lib/export.php';

$connect = db_connect();
include_once 'page/header.html';
include_once 'page/menu.php';


if (!empty($_SESSION['alert'])) {
    if (!empty($_SESSION['alert-color'])
        && in_array($_SESSION['alert-color'], ['danger', 'info', 'success', 'warning']) // white-list
    ) {
        $alertColor = $_SESSION['alert-color'];
    } else {
        $alertColor = 'danger';
    }
    echo '<div class="alert alert-' . $alertColor . '">' . $_SESSION['alert'] . '</div>';
    // only once
    unset($_SESSION['alert']);
}

// Vérifier la présence du paramètre content dans l'url
if (!empty($_GET['content'])) {
    getContent($_GET['content']);
}

include_once 'page/footer.html';