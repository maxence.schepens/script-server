<?php

const ROOT_PATH = __DIR__;

const FILE_EXT = ['php', 'html'];

const FILE_EXT_IMG = ['jpeg', 'png', 'jpg'];

const DB_NAME = 'webex';

const DB_HOST = 'localhost';

const DB_USER = 'root';

const DB_PASSWORD = '';