<nav class="navbar navbar-expand-lg border border-black">
    <div class="container-fluid justify-content-center">
        <a class="navbar-brand" href="index.php">Accueil</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-link active" aria-current="page" href="index.php?content=page/list">Liste des cours</a>
                <a class="nav-link" href="index.php?content=page/profile">Profil</a>
                <?php
                if (!empty($_SESSION['userid'])) {
                    ?>
                    <a href="index.php?content=page/logout" class="nav-link">Logout</a>
                    <?php
                } else { ?>
                    <a href="index.php?content=page/login" class="nav-link">Login</a>
                    <?php
                }

                if (!empty($_SESSION['userid'])) {
                    $user = getProfile('id', $_SESSION['userid']);
                    if (!is_object($user)) {

                    } else {
                        if ($user->admin = 1) {
                            ?>
                            <a href="index.php?content=page/admin" class="nav-link">Admin</a>
                            <?php
                        }
                    }
                }
                ?>
            </div>
        </div>
    </div>
</nav>