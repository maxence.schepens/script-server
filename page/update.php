<?php

if (!empty($user)) {

    $output = '             
<div class="accordion mleft mright" id="accordionExample">
  <div class="accordion-item">
    <h2 class="accordion-header">
      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
        Mise à jour du profil
      </button>
    </h2>
    <div id="collapseOne" class="accordion-collapse collapse show" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <div class="">
                  <form action="index.php?content=app/update" method="post" enctype="multipart/form-data">
                    <div class="mb-3">
                        <input class="form-control" type="hidden" id="uu-userid" name="id" value="' . $user->id . '">
                        <label class="form-label" for="uu-email">Email</label>
                        <input class="form-control" type="email" id="uu-email" name="email" class="form-control" value="' . $user->email . '"><br>
                        <label class="form-label" for="uu-password">Mots de passe</label>
                        <input class="form-control" type="password" id="uu-password" name="password" class="form-control" value="Mots de passe"><br>
                        <label class="form-label" for="uu-photo">Photo de profil</label>
                        <input class="form-control" type="file" id="uu-photo" name="photo" accept="image/jpeg, image/png"><br>
                        <input type="submit" class="btn btn-primary">
                    </div>
                </div>
              </form>
          </div>
      </div>
    </div>
  </div>';

echo $output;

}


