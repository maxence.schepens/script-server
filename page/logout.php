<?php
if (function_exists('verifIndex') && function_exists('logout')){
    verifIndex();
    logout();
} else {
    header('Location: index.php');
    alert('Au revoir', 'warning');
    die;
}