<?php

$out = false;
if (!empty($_SESSION['userid'])) {
    $user = getProfile('id', $_SESSION['userid']);
    if (!is_object($user)){
        $out = true;
    }
} else {
    $out = true;
}

if ($out) {
    header('Location: index.php?content=page/login');
    die;
}

$output = '<h2 class="mt-4">Profil</h2>
<table class="rounded table t1 mleft">
    <thead>
        <tr>
            <th>Intitulé</th>
            <th>Valeur</th>
        </tr>
    </thead>
    <tbody>';

if (!empty($user)) {
    foreach ($user as $key => $value) {
        if ($key == 'password' || $key == 'id') {
            continue;
        } elseif ($key == 'lastlogin' || $key == 'created') {
            $value = date_format(new DateTime($value), 'd/m/Y H:i:s');
        } elseif ($key == 'admin') {
            $value = getStatus($value);
        }
        $output .= '<tr><th>' . ucfirst($key) . '</th><td>' . $value . '</td></tr>';
    }
    if (!empty($photo)) {
        $output .= '<tr><th>Photo</th><td><img src="' . $photo . '" alt="photo de profil" class="d-block img-fluid"></td></tr>';
    }
}
$output .= '</tbody></table>';
echo $output;

$button = '';
if (!empty($user)){
    $button = '
    <a href="index.php?content=app/export"><button class="mleft mt-2 mb-3 btn btn-primary" id="export" name="export" type="submit">Export</button></a>';
}
echo $button;

require_once 'update.php';
