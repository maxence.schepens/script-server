<?php
$data = getData('*', 'course');
$output =
    '<h2 class="mt-4">Liste de cours</h2>
<table class="rounded table t1 mleft">
    <thead>
        <tr>
            <th>Intitulé</th>
            <th>Valeur</th>     
        </tr>
    </thead>
    <tbody>';


foreach ($data as $value) {
    $output .= '<tr><th>' . ucfirst($value->name) . '</th><td>' . $value->code . '</td></tr>';
}

$output .= '</tbody></table>';

echo $output;