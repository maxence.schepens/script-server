<?php
$url = 'index.php?content=page/login';
if (!empty($_POST['username']) && ($_POST['password'])) {
    $user = getDataField('*', 'user', 'username', $_POST['username']);

    if (!is_object($user)) {
        alert('Connexion échoué ', 'danger');
        $url = 'index.php?content=page/login';
        header('Location: ' . $url);
        die();
    }

    if (password_verify($_POST['password'], $user->password)) {
        // Stock id de l'user en session une fois que le mdp a été vérifier
        if(!empty($user->id)) {
            $_SESSION['userid'] = $user->id;
            $sql = "UPDATE user SET lastlogin = NOW() WHERE id = ?";
            $connect = db_connect();
            $update = $connect->prepare($sql);

            $update->execute([$user->id]);

            if ($update->rowCount()) {
                echo 'Précedente connexion : ' . $user->lastlogin;
            }

        }
        $url = 'index.php';
    } else {
        alert('Connexion échouée', 'danger');
        $url = 'index.php?content=page/login';
    }
    header('Location: ' . $url);
    alert('Bienvenue ' . $user->username . ' ', 'success');
}