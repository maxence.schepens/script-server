<?php

$out = false;
if (!empty($_SESSION['userid'])) {
    $user = getProfile('id', $_SESSION['userid']);
    if (!is_object($user)) {
        $out = true;
    }
} else {
    $out = true;
}

if ($out) {
    header('Location: index.php?content=page/login');
    die;
}

export($user, 'json');