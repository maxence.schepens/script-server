<?php



$out = false;
if (!empty($_SESSION['userid']) && $_SESSION['userid'] == $_POST['id']) {
    $user = getProfile('id', $_SESSION['userid']);
    if (!is_object($user)) {
        $out = true;
    }
} else {
    $out = true;
}

if ($out) {
    header('Location: index.php?content=page/list');
    die;
}
if (!empty($_SESSION['userid']) && is_object($user)) {
    if ($_SESSION['userid'] == $user->id) {
        //Expression ternaire (if condition ? true : false ;) remplacement de if else
        //if(isset($_POST['email'])) {
        //  $updateemail = 'email = :email';
        //} else {
        //  $updateemail = '';
        //}
        $updateemail = isset($_POST['email']) ? 'email = :email' : '';
        $updatepwd = isset($_POST['password']) ? 'password = :password' : '';
        $requirecoma = isset($_POST['email']) && isset($_POST['password']) ? ',' : '';
        $params = [
            'email' => $_POST['email'],
            'password' => password_hash($_POST['password'], PASSWORD_DEFAULT),
            'id' => $user->id
        ];
        $connect = db_connect();
        $sql = $connect->prepare("UPDATE user SET " . $updateemail . $requirecoma . $updatepwd . " WHERE id = :id "); // : = Récuperer une variable en SQL
        $sql->execute($params);
        if ($sql->rowCount()) {
            alert('Modification réussi', 'success');
        } else {
            alert('Modification échouée', 'danger');
        }
    }
}
header('Location: index.php');